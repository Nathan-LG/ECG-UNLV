# -*- coding: utf-8 -*-

import com
import csv
from time import sleep, time
from tkinter import *
import matplotlib.pyplot as plt
import numpy as np
import scipy as sp

HRDetection = 30           # R detection
maxTimeBetweenQRS = 50     # ms
SAVE_PATH = '/files/Fichiers/'

class ECG():

	def __init__(self, ui):
		self.points = []
		self.lines = []
		self.BPM = [0] * 12
		self.last = False
		self.HRTimestamp = []
		self.ui = ui
		self.dataX = []
		self.dataY = []
		self.maxX = 20000

	def start(self):
		self.ui.connected()

	def newPoint(self, x, y):
		self.points.append([int(x), int(y.strip())])
		self.dataY.append(float(self.points[-1][1]))
		self.dataX.append(self.points[-1][0])

		if (self.points[-1][1] < HRDetection and self.last == False):
			self.BPMCalc(int(x))
			self.last = True
		elif (self.points[-1][1] >= HRDetection and self.last == True):
			self.last = False

		if (len(self.points) == 1):
			self.startTime = time()

		delay = abs(int(x) - (time() - self.startTime) * 1000)

		self.ui.dispTime(str(int(delay)))

		if (len(self.points) > 1):

			x1 = ((self.points[-1][0] * 1000) / 5000) % 1000
			y1 = (self.points[-1][1] * 190) / 512 + 5
			x2 = ((self.points[-2][0] * 1000) / 5000) % 1000
			y2 = (self.points[-2][1] * 190) / 512 + 5

			if (x2 - x1 >= 0):
				self.ui.ecgC.delete('all')
			else:
				self.lines.append(self.ui.createLine(x1, y1, x2, y2))

	def plot(self):
		self.ax = plt.gca()
		self.ax.set_xlim(0,10000)
		self.ax.set_ylim(1024, 0)
		self.line, = self.ax.plot(self.dataX, self.dataY, 'r*-', label='ECG')

		# Diff (numpy)

		derivateY = np.diff(np.array(self.dataY))
		derivateX = np.diff(np.array(self.dataX))

		derivate = derivateY / derivateX
		derivate = np.insert(derivate, 0, 0)

		self.ax.plot(self.dataX, derivate, color='#A8A8A8', marker='x', label='Derivative')

		# Zero crossing for diff

		zeroCrossingsNum = np.where(np.diff(np.signbit(derivate)))[0]
		zeroCrossingsX = []
		zeroCrossingsY = []

		R = []
		Q = []
		S = []
		P = []
		T = []

		for idx, val in enumerate(self.dataY):
			if idx in zeroCrossingsNum:
				zeroCrossingsX.append(self.dataX[idx])
				zeroCrossingsY.append(val)

		for idx, val in enumerate(zeroCrossingsY):

			if (val < HRDetection):

				# R

				if (self.dataY[zeroCrossingsNum[idx] + 1] < HRDetection / 2):
					R.append((zeroCrossingsX[idx] + self.dataX[zeroCrossingsNum[idx] + 1]) / 2)
				else:
					R.append(zeroCrossingsX[idx])
				
				# Q

				if (idx > 1):
					if (zeroCrossingsY[idx - 1] >= HRDetection and zeroCrossingsX[idx] - zeroCrossingsX[idx - 1] <= maxTimeBetweenQRS):
						Q.append(zeroCrossingsX[idx - 1])
					elif (zeroCrossingsY[idx - 2] >= HRDetection and zeroCrossingsX[idx] - zeroCrossingsX[idx - 2] <= maxTimeBetweenQRS):
						Q.append(zeroCrossingsX[idx - 2])
			
				# S

				if (idx < len(zeroCrossingsX) - 1):
					if (zeroCrossingsY[idx + 1] >= HRDetection and zeroCrossingsX[idx + 1] - zeroCrossingsX[idx] <= maxTimeBetweenQRS):
						S.append(zeroCrossingsX[idx + 1])
					elif (zeroCrossingsY[idx + 2] >= HRDetection and zeroCrossingsX[idx + 2] - zeroCrossingsX[idx] <= maxTimeBetweenQRS):
						S.append(zeroCrossingsX[idx + 2])

		self.ax.plot(zeroCrossingsX, zeroCrossingsY, 'g+', mew=2, ms=5, label='Zero-crossings')
			
		zeroCrossingsPT = []
		SLeft = S[0]
		QRight = Q[1]

		# P and T

		for idx, val in enumerate(zeroCrossingsX):
			if (val in S):
				if (len(zeroCrossingsPT) != 0):
					minLeft  = 10000
					minRight = 10000
					for x in zeroCrossingsPT:
						if (x <= (SLeft + QRight) / 2 and x > SLeft):
							if (zeroCrossingsY[zeroCrossingsX.index(x)] < minLeft):
								minLeft = zeroCrossingsY[zeroCrossingsX.index(x)]
								idxLeft = zeroCrossingsX[zeroCrossingsX.index(x)]
						elif (SLeft < x < QRight):
							if (zeroCrossingsY[zeroCrossingsX.index(x)] < minRight):
								minRight = zeroCrossingsY[zeroCrossingsX.index(x)]
								idxRight = zeroCrossingsX[zeroCrossingsX.index(x)]
					try:
						T.append(idxLeft)
					except:
						pass
					
					try:
						P.append(idxRight)
					except:
						pass

				zeroCrossingsPT = []

				if (S.index(val) + 1 < len(Q)):
					SLeft = val
					QRight = Q[S.index(val) + 1]
			else:
				zeroCrossingsPT.append(val)

		# Plotting

		for val in Q:
			self.ax.axvline(val, color='#41F4DF')
	
		for val in R:
			self.ax.axvline(val, color='#F442C5')
	
		for val in S:
			self.ax.axvline(val, color='#7CF441')
	
		for val in P:
			self.ax.axvline(val, color='#D3872A')
	
		for val in T:
			self.ax.axvline(val, color='#203DE5')

		self.ax.legend()

		with open(SAVE_PATH + 'results' + str(time()) + '.csv', mode='w') as CSVFile:
			writer = csv.writer(CSVFile, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
			writer.writerow(['date', 'type'])

			for val in Q:
				self.ax.axvline(val, color='#41F4DF')
				writer.writerow([val, 'Q'])
		
			for val in R:
				self.ax.axvline(val, color='#F442C5')
				writer.writerow([val, 'R'])
		
			for val in S:
				self.ax.axvline(val, color='#7CF441')
				writer.writerow([val, 'S'])
		
			for val in P:
				self.ax.axvline(val, color='#D3872A')
				writer.writerow([val, 'P'])
		
			for val in T:
				self.ax.axvline(val, color='#203DE5')
				writer.writerow([val, 'T'])

		plt.show()
		plt.pause(1e-17)

	def BPMCalc(self, t):
		self.BPM = [t] + self.BPM[:-1]

		if (self.BPM[-1] != self.BPM[-2]):
			self.ui.dispBPM(str(int(300000 / (self.BPM[0] - self.BPM[5]))))